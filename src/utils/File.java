package utils;

public class File  extends Inode{
	 public int flag ;

	String content;
	private String child;

	public File(){}
	public File(String name , String content)
	{
		this.name = name;
		this.child = null;
		this.flag = 0 ;
		setContent(content);
	}
	
	public void setContent(String content)
	{
		this.content = content;
	}
	public String getContent()
	{
		return content;
	}

}
