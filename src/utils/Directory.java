package utils;
import utils.Inode;
import shell.shell;

public class Directory extends Inode {
	
	public Directory(){}
	public Directory(String name)
	{
		super.next=null;
		this.child=null;
		super.name=name;
		super.flag = 1;
	}
	
	public void add(Inode f)
	{
		f.next=this.child;
		this.child = f;
		f.parent=this;

	}
	static public void delete(Inode trash)
	{
		if(trash.parent.child == trash)
		{
			trash.parent.child = trash.next;
			return;
		}
		Inode node = trash.parent.child;
		Inode prev = node;
		while(node != trash)
		{
			prev = node;
			node = node.next;
		}
		prev.next = node.next;
		node.next = null;
	}

	public static Directory findInDirectory(Inode root , String name)
	{
		Inode node = root.child;
		while(node!= null)
		{
			if(node.getName().equals(name))
				return (Directory) node;
			node = node.next;
		}
		return null;
	}

	public static Directory resolvePath(shell s , Inode root , String path)
	{
		// System.out.println("Path : " + path);
		if(path.startsWith("/"))
		{
			root = s.root;
			path = path.substring(1, path.length());
			return resolvePath(s , root , path);
		}
		if(path.length()==0)
		{
			return (Directory) root;
		}
		if(path.indexOf("/") == path.length()-1 && !path.startsWith("."))
		{
			path = path.substring(0,path.length()-1);
			return findInDirectory(root,path);
		}

		if(path.startsWith("../"))
		{
			
			root = root.parent;
			path = path.substring(3,path.length());
			System.out.println();
		}
		else if(path.startsWith("./"))
		{
			path = path.substring(2,path.length());
		}
		else
		{
		root = findInDirectory(root, path.substring(0, path.indexOf('/')));
		path = path.substring(path.indexOf('/') +1, path.length());
		}

		return resolvePath(s,root, path);
	}	
}

