package shell.bin;
import utils.Inode;
import shell.*;
public class Pwd {
	public String path;
	shell s ;
	public Pwd(shell s)
	{
		path = "";
		this.s = s;
		System.out.println(getPath(s.cwd));
	}
	
	String getPath(Inode f)
	{
		if(f == s.root)
		{
			path+=s.root.getName();
			return path;
		}
		getPath(f.parent);
		path += f.getName();
		path += "/";

		return path;
	}
}
