package shell.bin;
import utils.*;

import java.util.Scanner;
import shell.shell;

public class Cat {

    File node ;
    public Cat(){}
    public Cat(shell s , String[] argv)
    {
        if(! checkArg(argv))
        {
            System.out.println("Invalid Amount of Arguemnt ");
            return;
        }

        Inode node = s.cwd.child;
        while(node!= null)
        {
            if(node.getName().equals(argv[1]) && node.getFlag() == 0)
                 {
                    break;
                 }
                node = node.next;
         }
         if(node == null)
         {
             System.out.println("File not Found !");
             return;
         }
         System.out.println(((File)node).getContent());
      
    }
    public  boolean checkArg(String[] argv)
    {
        if(argv.length < 2)
         return false;
        return true;
    }
}