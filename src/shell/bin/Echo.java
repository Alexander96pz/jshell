package shell.bin;
import utils.*;

import java.util.Scanner;

import shell.shell;

public class Echo {

    public Echo(){}
    public Echo(shell s , String[] argv)
    {
        if(! checkArg(argv))
        {
            System.out.println("Invalid Amount of Arguemnt ");
            return;
        }
        System.out.println("Content : ");
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\\s*EOF\\s*");

        String inp = scanner.next();
        s.cwd.add(new File(argv[1],inp));
    }
    public  boolean checkArg(String[] argv)
    {
        if(argv.length < 2)
         return false;
        return true;
    }
}
