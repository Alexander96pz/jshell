package shell.bin;
import utils.*;


public class Ls {
	 public Ls(Inode file)
	 {
		 if(file.getFlag() == 0)
	 	{
			listFile(file);
		}
		 else if (file.getFlag()== 1)
		{
			listDir(file);
		}
	 }
	 void listDir(Inode dir)
	 {
	
		Inode node = dir.child;
		System.out.print(".\t..\t");
		while(node!=null)
		{
			System.out.print(node.getName() + "\t");	
			node = node.next;
		}
		System.out.println();
		 
	 }
	 
	 void listFile(Inode file)
	 {
		 System.out.println(file.getName());
	 }
}
