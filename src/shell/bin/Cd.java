package shell.bin;
import utils.*;
import shell.shell;

public class Cd {

    Directory root ;
    public Cd(){}
    public Cd(shell s,String[] argv)
    {
        this.root = s.cwd;
        if(! checkArg(argv))
        {
            System.out.println("Invalid Amount of Arguemnt ");
            return;
        }

        if(argv[1].charAt(argv[1].length()-1)!='/')
        {
            argv[1] +="/"; 
        }
        System.out.println(argv[1]);
      Directory path = Directory.resolvePath(s,root,argv[1]);
      if(path == null)
      {
          System.out.println("File not Found !");
      }
      else
      {
          s.cwd=path;
      }
    }

    public  boolean checkArg(String[] argv)
    {
        if(argv.length < 2)
         return false;
        return true;
    }
}