package shell.bin;
import utils.*;
import shell.shell;
import java.lang.*; 
import java.io.*; 
import java.util.*; 


public class Mkdir {

    Directory parent;
    String name;
    String parentName;
    public Mkdir(){}
    public Mkdir(shell s , String[] argv)
    {
        parent = s.cwd;
        if(! checkArg(argv))
        {
            System.out.println("Invalid Number of Arguemnts ");
            return;
        }

        if(argv[1].indexOf("/")==-1)
        {
                s.cwd.add(new Directory(argv[1]));
                return;
        }
        else if(argv[1].charAt(argv[1].length()-1)!='/')
        {
            argv[1] +="/"; 
        }
        parsePath(argv[1]);

        System.out.println("Parent : " + parentName);
        System.out.println("File  : " +  name);
        parent = Directory.resolvePath(s,s.cwd, parentName);
        parent.add(new Directory(name));
    }

    public  boolean checkArg(String[] argv)
    {
        if(argv.length < 2)
         return false;
        return true;
    }

    public void parsePath(String path)
    {
        StringBuilder input = new StringBuilder();
        input.append(path.substring(0,path.length()-1));
        input = input.reverse();
        name = input.substring(0,input.indexOf("/"));
        name = new StringBuilder(name).reverse().toString();
        input = new StringBuilder(input.substring(input.indexOf("/"), input.length()));
        parentName = input.reverse().toString();
    }
}