package shell;
import java.util.Scanner;
import utils.*;
import shell.bin.*;

public class shell {
	
	 public Directory root = new Directory("/");
	 public Directory cwd ;

	public shell()
	{
		initialize();
	}
	public void initialize()
	{
		root.add(new Directory("home"));
		root.add(new Directory("etc"));
		cwd = this.root;
	}
	
	public void runShell()
	{
		while(true)
		{
			printPromt();
			String[] cmd = getCommand();

			switch(cmd[0])
			{
			  case "ls" : new Ls(cwd);break;
			  case "pwd" : new Pwd(this); break;
			  case "mkdir": new Mkdir(this,cmd);break;
			  case "cd" : new Cd(this,cmd);break;
			  case "echo" : new Echo(this,cmd);break;
			  case "cat" : new Cat(this,cmd);break;
			  case "rm" : new Rm(this,cmd);break;
			  case "mv" : new Mv(this,cmd);break;
			  case "exit": return;
			  default : break;
			}
			
		}	
	
	}

	public void printPromt()
	{
		System.out.print("$ ");
	}

	public String[] getCommand()
	{
		Scanner scanner = new Scanner(System.in);
		String inp = scanner.nextLine();
		String[] cmd = inp.split("[ \t]");
		return cmd;
	}
}
